/**
 * @param {string} text - the string containing your template.
 * @param {object} variables - containing the values to substitute in place of the keys enclosed by { and }.
 */

function escapeRegExp(str) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

function process(text, variables) {
  const regexp = new RegExp(`(\\\\\\{)|\(\(?=\\\\\)\(?:\\\\\\\\\)\)*${Object.keys(variables).map(k => escapeRegExp("{" + k + "}")).join('|')}`, 'gi')
  return text.replace(regexp, s => s[0] === "\\" ? s[1] : (variables[(s.slice(1, -1) || '').toLocaleLowerCase()] || '').toString());
}

module.exports = {
  process
}